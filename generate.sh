# Label images by hand

botkey=fixme

for img in *.png; do feh $img & printf 'Emoji? '; read emoji; printf 'Alt? '; read alt; echo "$img   $emoji  $alt" >> imginfo; killall feh; done

# Resize to sticker-compatible size
for img in *.png; do convert $img -resize 512x512 stickers/$img; done

# Use pngquant <png>, if image is >500K big

# Upload sticker files to telegram
cat imginfo | while read line; do file=$(echo $line | cut -f 1); printf "$file   " >> tgfiles; http --ignore-stdin --form https://api.telegram.org/bot$botkey/uploadStickerFile user_id=587689623 sticker@stickers/$file sticker_format=static | tee | jq '.result.file_id' >> tgfiles; done

# Join uploaded IDs with image data
join <(sort imginfo) <(sort tgfiles) -1 1 -2 1 -t "  " > tgdatafiles

# Create a json file with the first 50 stickers
cat tgdatafiles | head -n 50 | jq -R '[inputs | split("\t") | {sticker: .[3], emoji_list: .[1]} | .emoji_list |= split(",")]' > stickerlist.json

# Create a sticker set with first 50 stickers
http --ignore-stdin https://api.telegram.org/bot$botkey/createNewStickerSet user_id=587689623 name=memozrnnost_by_GrnScrnrBot title="\@memozrnnost stickers by @GrnScrnr" sticker_format=static stickers:=@stickerlist.json

# Upload the rest
i=51
while true; do ((i++)); http --ignore-stdin https://api.telegram.org/bot$botkey/addStickerToSet user_id=587689623 name=memozrnnost_by_GrnScrnrBot sticker:="$(cat tgdatafiles | head -n $i | tail -n 1 | jq -R 'split("\t") | {sticker: .[3], emoji_list: .[1]} | .emoji_list |= split(",")')"; done
