# @memozrnnost archives

Archiv všech memíků, které tvítnul účet [@memozrnnost](https://twitter.com/memozrnnost). Plus soubor s textovými popisy všech obrázků a postup pro vygenerování telegramovýho stickerpacku.

[Vygenerovaný stickerpack](https://t.me/addstickers/memozrnnost_by_GrnScrnrBot)
