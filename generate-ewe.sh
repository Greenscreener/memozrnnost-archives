mkdir -p ewe/memozrnnost
i=0
cat imginfo | while read sticker;
do
	filename=$(echo $sticker | cut -f 1)
	emoji=$(echo $sticker | cut -f 2)
	alt=$(echo $sticker | cut -f 3)
	number=$((i++))
	target=$(echo "ewe/memozrnnost/$number - $emoji - $alt.webp" | sed 's/["!?:<>]/_/g' | sed 's/png$/webp/g')
	magick "$filename" -resize 510x510 -background transparent -gravity center -extent 512x512 "$target"
done
